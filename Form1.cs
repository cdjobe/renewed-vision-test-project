﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace testproject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

            // compare the parsed characters/symbols to regex
            Regex reg = new Regex(@"([#|@]{1}[a-z]+\z)");

            string[] linesSplit = richTextBox1.Lines;

            // currently matches as entered. needs a way to deselect prevoius matches if they become unacceptable strings
            foreach (Match match in reg.Matches(richTextBox1.Text))
                {
                    int i = richTextBox1.SelectionStart;
                    richTextBox1.SelectionStart = match.Index;
                    richTextBox1.SelectionLength = richTextBox1.Text.Length;
                    richTextBox1.SelectionColor = Color.Red;
                    richTextBox1.SelectionStart = i;

                    //Console.WriteLine("true");
                    //Console.WriteLine(match);
                }

            richTextBox1.SelectionColor = Color.Black;
        }


    }
}
